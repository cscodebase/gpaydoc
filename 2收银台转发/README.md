
有2个做法，请尽量使用第二种方法。
不管选择哪个做法，都要把配置了这个的server的域名，端口，http/https 告诉我们，我们需要改break_url。 


一、全转发版，只能凑合用，把下面这4条转发规则配置在一个面向客户的nginx上。

```
location /ui/ {
    proxy_pass http://gp.llque.com/ui/;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}
location /static-pay/ {
    proxy_pass http://gp.llque.com/static-pay/;
    proxy_set_header X-Real-IP $remote_addr;
### 可以cache
}
location /track {
    proxy_pass http://gp.llque.com/track;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}
location /api/juyifu-pay-info {
    proxy_pass http://gp.llque.com/api/juyifu-pay-info;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}
```

二、只转发动态内容，不转发静态内容，这种方式能给客户提供最好的体验，把下面这3条转发规则配置在一个面向客户的nginx上。

```
location /ui/ {
    proxy_pass http://gp.llque.com/ui/;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}

location /track {
    proxy_pass http://gp.llque.com/track;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}
location /api/juyifu-pay-info {
    proxy_pass http://gp.llque.com/api/juyifu-pay-info;
    proxy_set_header X-Real-IP $remote_addr;
### 不要cache
}
location /static-pay/ {
    root /var/gpay-static/;

}

```
并把gpay-static 放到/var 下。
 