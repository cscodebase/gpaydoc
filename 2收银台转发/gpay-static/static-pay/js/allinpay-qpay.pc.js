var queryInternal = 5000;
var queryTimerId;


// 超时。
var secondAll = 60 * 10;
var clock = null;

// 绑卡。
var bindCardRequestId = null;
var currentSelectedBindCard = null;
var payId = null;
var currentState = "";

$(window).load(function () {
    //设置计时器
    clock = setInterval(setTimer, 1000);

    //查询订单支付状态
    queryOrder();

    // 判断当前状态
    if (bindCards.length == 0) {
        setCurrentState('bind_card');
    } else {
        setCurrentState('pay');
    }

    // 直接支付
    $('.try_direct_pay').click(function () {
        setCurrentState('pay_direct');
    });

    // 签约
    $('.section_bind_card .fetch_smscode').click(bindCard);
    $('.section_bind_card .bind_card').click(bindCardConfirm);

    // 支付
    $('.section_pay .fetch_smscode').click(pay);
    $('.section_pay .pay_confirm').click(payConfirm);

    $('.section_pay .bind_cards_container .bind_another_card').click(function () {
        setCurrentState('bind_another_card');
    });

    // 签约新卡
    $('.section_bind_another_card .fetch_smscode').click(bindAnotherCard);
    $('.section_bind_another_card .bind_card').click(bindAnotherCardConfirm);

    // 直接支付
    $('.section_pay_direct .fetch_smscode').click(payDirect);
    $('.section_pay_direct .pay_confirm').click(payConfirmDirect);

    // $('.section_pay_direct .bind_cards_container .bind_another_card').click(function() {
    // 	setCurrentState('bind_another_card');
    // });

    // 显示银行卡限额
    $('.section_title_bank_quota').click(showBankQuota);
    $('.bank_card_quote_return').click(hideBankQuota);
});

// 校验参数是否正确输入
function showVerifyResult(input, verifyOk, errorMessage) {
    var inputs = input.parent();
    if (!$(inputs).hasClass('inputs')) {
        inputs = input.parent().parent();
    }
    var indicator = inputs.children('.valid-indicator');
    var error = $(inputs.parent().find('.error'));
    
    if(!verifyOk){
        error.text(errorMessage);
        error.show();
        indicator.hide();
        input.focus(); 
    } else {
        error.text("");
        error.hide();
        indicator.show();
    }
}

function verifyName(e){
    var input = $(e);
    var value = input.val();
    var verifyOk = value.length >= 2;
    showVerifyResult(input, verifyOk, '姓名长度大于等于两个字符');
    return verifyOk;
}

function idNum(e){
    let idNumReg = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
    var input = $(e);
    var value = input.val();
    var verifyOk = idNumReg.test(value);
    showVerifyResult(input, verifyOk, '请输入正确的身份证号');
    return verifyOk;
}

function bankNum(e){
	/*
    let bankNumReg = /^([1-9]{1})(\d{14}|\d{18})$/;
    var input = $(e);
    var value = input.val();
    var verifyOk = bankNumReg.test(value);
    showVerifyResult(input, verifyOk, '请输入正确的银行卡号');
    return verifyOk;
    */
	return true;
}

function phoneNum(e){
    let phoneNumReg = /^1(3|4|5|6|7|8|9)\d{9}$/;
    var input = $(e);
    var value = input.val();
    var verifyOk = phoneNumReg.test(value);
    showVerifyResult(input, verifyOk, '请输入正确的手机号');
    return verifyOk;
}

function code(e){
    var input = $(e);
    var value = input.val();
    var verifyOk = value.length >= 2;
    showVerifyResult(input, verifyOk, '请输入正确的验证码');
    return verifyOk;
}

var countdown = 60; 
function settime(val) {
    if (countdown == 0) {    
        val.prop("disabled", false);
        val.css("background","#00828B"); 
        val.val("获取验证码"); 
        countdown = 60; 
        clearTimeout(time);
    } else { 
        val.attr("disabled", true); 
        val.css("background","#666666");
        val.val(countdown + "s可重新发送"); 
        countdown --; 
    } 
    let time = setTimeout(function() { 
        settime(val) 
    },1000);
} 

//
function showBankQuota() {
    $('.content-pay').hide();
    $('.bank_card_quota').show();
}

function hideBankQuota() {
    $('.content-pay').show();
    $('.bank_card_quota').hide();
}

// 处理绑卡请求 - 校验请求参数。
function bindCardValidate(sectionSelector, checkSmsCode) {
    var acctno = $(sectionSelector + '.acctno input'),
    acctname = $(sectionSelector + '.acctname input'),
    idno = $(sectionSelector + '.idno input'),
    mobile = $(sectionSelector + '.mobile input'),
    smscode = $(sectionSelector + '.smscode input');
    
    var valid = verifyName(acctname) && idNum(idno) && bankNum(acctno) && phoneNum(mobile);

    if (checkSmsCode) {
        valid = valid && code(smscode);
    }

    if (valid) {
        return {
            'valid': true,
            'acctno': acctno.val(),
            'acctname': acctname.val(),
            'idno': idno.val(),
            'mobile': mobile.val(),
            'smscode': smscode.val()
        };
    } else {
        return {
            'valid': false
        };
    }
}

//
function showTips(selector, content) {
    var element = $(selector);
    element.text(content);
    element.show();
}

// 处理绑卡请求。
function bindCardBase(t, sectionSelector, handler) {
    t.unbind('click');
    console.log('bind card');
    var validateResult = bindCardValidate(sectionSelector, false);

    if (!validateResult.valid) {
        t.bind('click', handler);
        return;
    }
    
    settime(t);

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/bind-card/ap',
        data: {
            'order_no': orderNo,
            'acctno': validateResult.acctno,
            'acctname': validateResult.acctname,
            'idno': validateResult.idno,
            'mobile': validateResult.mobile,
            'bind_request_id': bindCardRequestId
        },
    }).done(function (response) {
        if (response.code == 200) {
            showTips(sectionSelector + '.error', '申请成功，请注意查收验证码短信！');
            bindCardRequestId = response.bind_request_id;
        } else if (response.code == 4201) {
            setCurrentState('pay_direct');
            $('.section_pay_direct .acctno input').val($('.section_bind_card .acctno input').val());
            $('.section_pay_direct .mobile input').val($('.section_bind_card .mobile input').val())
        } else {
            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', handler);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request bind-card');
        showTips(sectionSelector + '.error', '请求签约失败，请重试！');
        t.bind('click', handler);
    });
}

function bindCard() {
    bindCardBase($(this), '.section_bind_card ', bindCard);
}

function bindAnotherCard() {
    bindCardBase($(this), '.section_bind_another_card ', bindAnotherCard);
}

// 处理绑卡确认。
function bindCardConfirmBase(t, sectionSelector, handler) {
    t.unbind('click');
    console.log('bind card confirm');
    var validateResult = bindCardValidate(sectionSelector, true);

    if (!validateResult.valid) {
        t.bind('click', handler);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/bind-card-confirm/ap',
        data: {
            'order_no': orderNo,
            'acctno': validateResult.acctno,
            'acctname': validateResult.acctname,
            'idno': validateResult.idno,
            'mobile': validateResult.mobile,
            'bind_request_id': bindCardRequestId,
            'smscode': validateResult.smscode
        },
    }).done(function (response) {
        if (response.code == 200) {
            bindCards = response.data;
            setCurrentState('pay');
        } else if (response.code == 4201) {
            setCurrentState('pay_direct');
            $('.section_pay_direct .acctno input').val($('.section_bind_card .acctno input').val());
            $('.section_pay_direct .mobile input').val($('.section_bind_card .mobile input').val())
        } else {
            // 这种场景应该是签约信息变了，需要重新获取短信验证码。
            if (response.code == 4205) {
                bindCardRequestId = response.bind_request_id;
            }

            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', bindCardConfirm);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request bind-card');
        showTips(sectionSelector + '.error', '签约确认失败，请重试！');
        t.bind('click', bindCardConfirm);
    });
}

function bindCardConfirm() {
    bindCardConfirmBase($(this), '.section_bind_card ', bindCardConfirm);
}

function bindAnotherCardConfirm() {
    bindCardConfirmBase($(this), '.section_bind_another_card ', bindAnotherCardConfirm);

}

// 支付参数校验。
function payValidate(sectionSelector, checkAcctno, checkSmsCode) {
    var acctno = $(sectionSelector + '.acctno input'),
    mobile = $(sectionSelector + '.mobile input'),
    smscode = $(sectionSelector + '.smscode input');
    var valid = true;

    if (checkAcctno) {
        valid = valid && bankNum(acctno) && phoneNum(mobile);
    }

    if (checkSmsCode) {
        valid = valid && code(smscode);
    }

    if (valid) {
        return {
            'valid': true,
            'acctno': acctno.val(),
            'mobile': mobile.val(),
            'smscode': smscode.val()
        };
    } else {
        return {
            'valid': false
        };
    }
}

// 支付。
function pay() {
    var t = $(this);
    var sectionSelector = '.section_pay ';

    t.unbind('click');
    console.log('pay request');

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/pay/ap',
        data: {
            'order_no': orderNo,
            'bind_card_id': currentSelectedBindCard.id,
            'pay_id': payId
        },
    }).done(function (response) {
        if (response.code == 200) {
            showTips(sectionSelector + '.error', '请求成功，请查收验证码短信！');
            payId = response.pay_id;
        } else {
            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', pay);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request pay');
        showTips(sectionSelector + '.error', '请求支付失败，请重试！');
        t.bind('click', pay);
    });
}

// 支付确认。
function payConfirm() {
    var t = $(this);
    var sectionSelector = '.section_pay ';

    t.unbind('click');
    console.log('pay-confirm request');
    $(sectionSelector + '.tips .section_item_warning').hide();

    var validateResult = payValidate(sectionSelector, false, true);
    if (!validateResult.valid) {
        t.bind('click', payConfirm);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/pay-confirm/ap',
        data: {
            'order_no': orderNo,
            'bind_card_id': currentSelectedBindCard.id,
            'pay_id': payId,
            'smscode': validateResult.smscode
        },
    }).done(function (response) {
        if (response.code == 200) {
            showTips(sectionSelector + '.error', '支付确认成功！请等待支付完成！');
            if (payId == null) {
                payId = response.pay_id;
            }
            
            $('.section_pay_success .acctname .detail').text(response.acctname);
            $('.section_pay_success .idno .detail').text(response.idno);
            $('.section_pay_success .acctno .detail').text(response.acctno);
            $('.section_pay_success .mobile .detail').text(response.mobile);
            
            unbindAllPayEventHandlers();
        } else {
            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', payConfirm);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request pay-confirm');
        showTips(sectionSelector + '.error', '支付确认失败，请重试！');
        t.bind('click', payConfirm);
    });
}

// 支付。
function payDirect() {
    var t = $(this);
    var sectionSelector = '.section_pay_direct ';

    t.unbind('click');
    console.log('pay request');

    $(sectionSelector + '.error').hide();

    var validateResult = payValidate(sectionSelector, true, false);
    if (!validateResult.valid) {
        t.bind('click', payDirect);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/pay/ap',
        data: {
            'order_no': orderNo,
            'acctno': validateResult.acctno,
            'mobile': validateResult.mobile,
            'pay_id': payId
        },
    }).done(function (response) {
        if (response.code == 200) {
            showTips(sectionSelector + '.error', '请求成功，请查收验证码短信！');
            payId = response.pay_id;
        } else {
            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', payDirect);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request pay');
        showTips(sectionSelector + '.error', '请求支付失败，请重试！');
        t.bind('click', payDirect);
    });
}

// 支付确认。
function payConfirmDirect() {
    var t = $(this);
    var sectionSelector = '.section_pay_direct ';

    t.unbind('click');
    console.log('pay-confirm request');
    $(sectionSelector + '.tips .section_item_warning').hide();

    var validateResult = payValidate(sectionSelector, true, true);
    if (!validateResult.valid) {
        t.bind('click', payConfirmDirect);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/api/v2/express-pay/pay-confirm/ap',
        data: {
            'order_no': orderNo,
            'acctno': validateResult.acctno,
            'mobile': validateResult.mobile,
            'pay_id': payId,
            'smscode': validateResult.smscode
        },
    }).done(function (response) {
        if (response.code == 200) {
            showTips(sectionSelector + '.error', '支付确认成功！请等待支付完成');
            if (payId == null) {
                payId = response.pay_id;
            }
            
            $('.section_pay_success .acctname .detail').text(response.acctname);
            $('.section_pay_success .idno .detail').text(response.idno);
            $('.section_pay_success .acctno .detail').text(response.acctno);
            $('.section_pay_success .mobile .detail').text(response.mobile);
            
            unbindAllPayEventHandlers();
        } else {
            showTips(sectionSelector + '.error', response.message);
        }
        t.bind('click', payConfirmDirect);
    }).fail(function (data, textStatus, jqXHR) {
        console.log('failed request pay-confirm');
        showTips(sectionSelector + '.error', '支付确认失败，请重试！');
        t.bind('click', payConfirmDirect);
    });
}

// 设置当前状态。
function setCurrentState(state) {
    var sections = ['.section_bind_card', '.section_pay', '.section_bind_another_card', '.section_pay_direct', '.section_pay_success'];
    for (var i = 0; i < sections.length; ++i) {
        if (sections[i] == '.section_' + state) {
            $(sections[i]).show();
        } else {
            $(sections[i]).hide();
        }
    }

    if (state == 'pay') {
        setBindCards('.section_pay .bind_cards_container .bind_card_list', bindCards, selectBindCard);
        setSelectedBindCard(getDefaultBindCard(bindCards));
    }

    if (state == 'bind_another_card') {
        setBindCards('.section_bind_another_card .bind_cards_container .bind_card_list', bindCards, selectBindCardForPay);
    }

    currentState = state;
}

// 设置已绑定卡列表。
function setBindCards(bindCardListSelector, bindCards, selectBindCard) {
    var bindCardList = $(bindCardListSelector);
    bindCardList.empty();
    for (var i = 0; i < bindCards.length; ++i) {
        var bindCard = bindCards[i];
        var bindItem = '<div class="bind_card_item" bindid="' + bindCard.id + '">\n' +
            '<div class="bind_card_item_card_no">' + bindCard.acctno + '</div>\n' +
            '<div class="bind_card_item_indicator">&gt;</div>\n' +
            '</div>';
        bindCardList.append($(bindItem));
    }
    $(bindCardListSelector + ' .bind_card_item').click(selectBindCard);
}

// 设置选择的卡。
function setSelectedBindCard(bindCard) {
    $('.section_pay .acctno .detail').text(bindCard.acctno);
    $('.section_pay .mobile .detail').text(bindCard.mobile);
    currentSelectedBindCard = bindCard;
}

// 获取默认卡。
function getDefaultBindCard(bindCards) {
    if (bindCards.length == 0) {
        return bindsCards[0];
    } else {
        for (var i = 0; i < bindCards.length; ++i) {
            var bindCard = bindCards[i];
            if (bindCard.is_default_card) {
                return bindCard;
            }
        }
        return null;
    }
}

// 获取选择的卡。
function getSelectedBindCard(bindid) {
    for (var i = 0; i < bindCards.length; ++i) {
        var bindCard = bindCards[i];
        if (bindCard.id == bindid) {
            return bindCard;
        }
    }
    return null;
}

// 选择用来支付的卡。
function selectBindCard() {
    var bindid = parseInt($(this).attr('bindid'));
    var bindCard = getSelectedBindCard(bindid);
    if (bindCard != null) {
        setSelectedBindCard(bindCard);
    }
}

// 选择卡并支付。
function selectBindCardForPay() {
    var bindid = parseInt($(this).attr('bindid'));
    var bindCard = getSelectedBindCard(bindid);
    setCurrentState('pay');
    if (bindCard != null) {
        setSelectedBindCard(bindCard);
    }
}

//设置计时器
function setTimer() {
    getTimeStr();
    $(".remaining_time").text(time_str);
}

//获取当前时间串
function getTimeStr() {
    secondAll--;
    if (secondAll == 0) {
        clearInterval(clock);
    }
    var minute = "0" + parseInt(secondAll / 60);

    var second = secondAll - minute * 60;
    if (second < 10) {
        second = "0" + second;
    }
    time_str = minute + "分" + second + "秒";
}

function unbindAllPayEventHandlers() {
    // 签约
    $('.section_bind_card .fetch_smscode').unbind();
    $('.section_bind_card .bind_card').unbind();

    // 支付
    $('.section_pay .fetch_smscode').unbind();
    $('.section_pay .pay_confirm').unbind();

    // 签约新卡
    $('.section_bind_another_card .fetch_smscode').unbind();
    $('.section_bind_another_card .bind_card').unbind();

    // 直接支付
    $('.section_pay_direct .fetch_smscode').unbind();
    $('.section_pay_direct .pay_confirm').unbind();
}

// 查询订单支付状态
function queryOrder() {
    $.ajax(apiDomain + "/api/order/query?order_no=" + orderNo)
    .done(function (data) {
        if (data.code != 200) {
            console.log('failed query order status, code: ' + data.code + ", message: " + data.message);
            setTimeout(queryOrder, queryInternal);
            return;
        }

        console.log('order status====> ' + data.order_status + " finished: " + data.order_finished);
        $('#pay_status').text(data.order_status);
        if (!data.order_finished) {
            setTimeout(queryOrder, queryInternal);
        } else {
            showTips('.section_' + currentState + ' .tips .section_item_warning', '支付已完成！');
            clearInterval(clock);
            unbindAllPayEventHandlers();
            setCurrentState('pay_success');
        }
    })
    .fail(function () {});
}