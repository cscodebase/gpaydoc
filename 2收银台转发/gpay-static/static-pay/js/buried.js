// https://gitlab.com/cscodebase/gpaygxn/-/issues/813 GPAY-记录客户访问收银台的详细信息-埋点
var baseUrl = '/track'

/**
 * 打开收银台事件
 * @param data
 */
function trackAccessCashierEvent(data) {

    var buried_point = {
        orderNo : data.orderNo,
        companyId: data.companyId,
        companyCode : data.companyCode,
        file : data.file,
        timestamp : new Date().getTime(),
        event: "2-access_cashier"
    }
    var buried_data = formatParams(buried_point);

    $.ajax({
        type:'POST',
        url : baseUrl + '?' + buried_data
    })

}

/**
 * 获得收款账户事件
 * @param data
 * @param start_time 打开收银台的时间
 */
function trackGetReceivingAccountEvent(data,start_time) {

    var buried_point = {
        orderNo : data.orderNo,
        companyId: data.companyId,
        companyCode : data.companyCode,
        file : data.file,
        latency : new Date().getTime() - start_time + 'ms',
        event: "3-get_receiving_account"
    }
    var buried_data = formatParams(buried_point);

    $.ajax({
        type:'POST',
        url : baseUrl + '?' + buried_data
    })

}


function formatParams(data) {
    data = JSON.stringify(data).replace(/\"/g, "").replace(/\,/g, "&").replace(/\:/g, "=");
    data = data.substring(1, data.length - 1);
    return data;
}
