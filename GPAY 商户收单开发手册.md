﻿# <center>GPAY 商户收单开发手册</center> #

<center>版本 2.2</center>


## 1. 名词解释

&emsp;&emsp;用户：是指在商户平台使用的客户。

&emsp;&emsp;商户：向支付平台发送充值提现申请需求。

&emsp;&emsp;支付平台：GPay支付。

&emsp;&emsp;静态密码：GPay支付公司提供的静态密码，参数中成为Config。

&emsp;&emsp;动态密钥：每个订单都需要动态密钥校验通过才可以申请成功，动态密钥组成见参数表。


----------


## 2. 接口标准
&emsp;&emsp;GPay支付平台，使用HTTP协议进行数据通信，请求数据以POST形式提交，响应方式是JSON格式。所有的请求与响应都使用UTF-8字符集进行编码。


----------


## 3. 接口规范
&emsp;&emsp;商户订单系统用订单号标识每笔订单，每笔订单都有唯一的订单号，支付平台收到订单申请，会对应返回支付平台订单号，由此决定唯一订单。

&emsp;&emsp;对于支付平台推送的每笔订单，商户都需要在5秒内做出应答，如果商户无响应，支付平台会再次推送，重复推送三次；如果商户还是没有及时响应，支付平台会每半个小时推送一次，直至48小时停止推送。由于存在相同订单号推送多次的可能，所以商户务必验证订单号，相同的订单号只应该处理一次。

&emsp;&emsp;不允许商户对支付平台的参数信息自行修改，比如增减参数中的“空格”“-”等符号。


----------


## 4. 支付方式
&emsp;&emsp;目前，GPay支持以下几种支付方式，并将分别描述几种支付方式的使用流程。

### 4.1. 微信扫码支付（话费）

&emsp;&emsp;客户通过微信扫码完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；

### 4.2. 网银转账
&emsp;&emsp;客户通过网银转账的方式完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；

### 4.3. 微信扫码
&emsp;&emsp;客户通过微信扫码的方式完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；


### 4.4. 支付宝扫码

&emsp;&emsp;客户通过支付宝扫码的方式完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；


### 4.5. USDT转账

&emsp;&emsp;客户通过扫码的方式完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；


### 4.6. 外币电汇

&emsp;&emsp;客户通过电汇的方式完成支付。

&emsp;&emsp;过程如下：

&emsp;&emsp;1. 商户向GPay发起支付请求，GPay向商户返回收银台链接；

&emsp;&emsp;2. 商户将收银台链接显示给客户；

&emsp;&emsp;3. 客户访问收银台链接，按照提示完成支付；

----------


## 5. 支付

服务器地址：​https://gapi.llque.com/api/v2/mownecum/pay_request

### 5.1. 请求消息

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id  | 商户ID：支付平台提供给商户的编码。 | 3 | 否 |
| bank_id | 银行ID：用户发起充值的银行ID，编码参见：银行编码。此参数用于选择银行。 | 2 | 可 |
| amount | 充值金额：数字，必须两位小数点，无千位符。 | 10 | 否
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否
| company_user | 商户客户唯一ID：客户在商户前台使用的唯一性ID，字符串，可使加密的用户ID,请务必提交正确的数据，否则会影响支付成功率。 | 64 | 否
| estimated_payment_bank | 客户预计使用银行：与bank_id一致。 | 2 | 可
| deposit_mode | 充值渠道：<br>24 - H5快捷支付<br>25 - 支付宝H5唤醒+PC扫码<br>26 - 支付宝商户扫码<br>27 - 微信扫码话费<br>28 - 微信固码扫码<br>29 - 网银转账<br>30 - 卡到卡（带附言）<br>31 - 卡到卡（无附言）<br>32 - 个码微信自由码<br>33 - 个码支付宝自由码<br>34 - C2C支付宝到银行卡<br>35 - 网银快捷<br>36 - 网银快捷（免签约）<br>37 - 微信H5<br>38 - C2C支付宝个码扫码（混合）<br>39 - 微信扫码混跑 <br>40 - H5快捷支付（一号一户）<br>41 - C2C微信自由码（混合）<br>42 - C2C微到卡<br>43 - C2C卡到卡（无附言）<br>44 - C2C卡到卡（带附言）<br>45 - C2C微信赞赏码<br>46 - C2C支付宝个码H5<br>51 - USDT转账<br>61 - USD电汇<br>71 - EUR电汇  | 1 | 否
| group_id | 群组ID：支付平台自用参数，建议商户默认0。 | 8 | 否
| web_url | 商户网址：建议商户可传网站域名。 | 128 | 否
| memo | 备用字段。 | 128 | 可
| note | 附言：如果note_model为1，note不能为空。 | 32 | 可
| note_model | 订单匹配模式：<br>1：商户自传附言<br>2：支付平台附言<br>3：支付平台金额 <br>4：商户金额。<br>目前，建议商户设置为2。 | 1 | 可
| key | 动态密钥：<br>MD5(MD5(config)+company_id+bank_id+<br>amount+company_order_num+company_user+<br>estimated_payment_bank+deposit_mode+<br>group_id+web_url+memo+note+note_model)<br>备注：config为静态密码。 | 32 | 否
| terminal | 使用终端：<br>1：电脑端<br>2：手机端<br>3：平板<br>9：其他 | 1 | 否
| client_ip | 客户终端IP。<br>这是发起支付的客户所用终端的IP地址，请务必提交真的IP地址，否则会影响支付成功率。 | 32 | 否
### 5.2. 应答消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
|  bank_card_num | 收款银行卡号：客户需要转账给此银行卡号。C2C银行卡转账时，此字段不可为空。  | MAX(32) | 可 |
|  bank_acc_name | 收款银行户名：客户需要转账给此银行户名。C2C银行卡转账时，此字段不可为空。  | 32 | 可 |
|  amount | 充值金额：数字，必须两位小数点，无千位符。  | 10 | 否 |
|  email | 收款Email账号：正确的Email格式，如果mode为2，则不能为空。  | 64 | 可 |
|  company_order_num | 商户订单号：唯一性的字符串  | 64 | 否 |
|  datetime | 订单有效时间：年月日时分秒。  | 14 | 否 |
|  note | 附言：用于订单匹配。  | 32 | 可 |
|  mownecum_order_num | 支付平台订单号：字符串，唯一。  | 64 | 否 |
|  status | 状态： <br>1：处理成功-success<br>0：处理失败-failed  | 1 | 否 |
|  error_msg | 错误信息：如果status为0，该参数需要显示信息。  | 128 | 可 |
|  mode | 收款方式：<br>1：银行账号 <br>2： Email收款  | 1 | 否 |
|  issuing_bank_address | 开户行地址：银行卡开户行地址。  | 64 | 可 |
|  break_url | 引用地址：银行卡充值该参数为空。  | 128 | 可 |
|  deposit_mode  | 充值渠道：<br>24 - H5快捷支付<br>25 - 支付宝H5唤醒+PC扫码<br>26 - 支付宝商户扫码<br>27 - 微信扫码话费<br>28 - 微信固码扫码<br>29 - 网银转账<br>30 - 卡到卡（带附言）<br>31 - 卡到卡（无附言）<br>32 - 个码微信自由码<br>33 - 个码支付宝自由码<br>34 - C2C支付宝到银行卡<br>35 - 网银快捷<br>36 - 网银快捷（免签约）<br>37 - 微信H5<br>38 - C2C支付宝个码扫码（混合）<br>39 - 微信扫码混跑 <br>40 - H5快捷支付（一号一户）<br>41 - C2C微信自由码（混合）<br>42 - C2C微到卡<br>43 - C2C卡到卡（无附言）<br>44 - C2C卡到卡（带附言）<br>45 - C2C微信赞赏码<br>46 - C2C支付宝个码H5<br>51 - USDT转账<br>61 - USD电汇<br>71 - EUR电汇  | 1 | 否 |
|  collection_bank_id |收款银行ID：收款银行卡编码。C2C银行卡转账时，此字段不可为空。| 2 | 可 |
|  collection_bank_name |收款银行名称：收款银行卡归属银行的名称。C2C银行卡转账时，此字段不可为空。  | 128 | 可 |
|  deposit_page   | 支付页面：B2C网银转账时，此字段不能为空，商户拿到此字段后，用此字段替换掉当前页面的内容。  | 4096 | 否 |
|  key | 动态密钥：<br>MD5(MD5(config)+bank_card_num+bank_acc_name+<br>amount+email+company_order_num+datetime+note+<br>mownecum_order_num+status+error_msg+mode+<br>issuing_bank_address+break_url+deposit_mode+<br>collection_bank_id)<br>备注：config为静态密码  | 32 | 否 |
### 5.3. 直接获得收款银行账户

目前仅支持 34-宝到卡 和 43-卡到卡(无附言) 两种支付方式

34-宝到卡 请求url:​https://gapi.llque.com/api/v2/mownecum/bdk_pay_request <br>
43-卡到卡 请求url:​https://gapi.llque.com/api/v2/mownecum/kdk_pay_request

#### 5.3.1. 请求消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id  | 商户ID：支付平台提供给商户的编码。 | 3 | 否 |
| bank_id | 银行ID：用户发起充值的银行ID，编码参见：银行编码。此参数用于选择银行。 | 2 | 可 |
| amount | 充值金额：数字，必须两位小数点，无千位符。 | 10 | 否
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否
| company_user | 商户客户唯一ID：客户在商户前台使用的唯一性ID，字符串，可使加密的用户ID,请务必提交正确的数据，否则会影响支付成功率。 | 64 | 否
| estimated_payment_bank | 客户预计使用银行：与bank_id一致。 | 2 | 可
| deposit_mode | 充值渠道：<br>34 - 宝到卡<br> 43 - C2C卡到卡(无附言)<br>所选渠道要对应请求的url<br>| 1 | 否
| group_id | 群组ID：支付平台自用参数，建议商户默认0。 | 8 | 否
| web_url | 商户网址：建议商户可传网站域名。 | 128 | 否
| memo | 备用字段。 | 128 | 可
| note | 附言：如果note_model为1，note不能为空。 | 32 | 可
| note_model | 订单匹配模式：<br>1：商户自传附言<br>2：支付平台附言<br>3：支付平台金额 <br>4：商户金额。<br>目前，建议商户设置为2。 | 1 | 可
| key | 动态密钥：<br>MD5(MD5(config)+company_id+bank_id+<br>amount+company_order_num+company_user+<br>estimated_payment_bank+deposit_mode+<br>group_id+web_url+memo+note+note_model)<br>备注：config为静态密码。 | 32 | 否
| terminal | 使用终端：<br>1：电脑端<br>2：手机端<br>3：平板<br>9：其他 | 1 | 否
| client_ip | 客户终端IP。<br>这是发起支付的客户所用终端的IP地址，请务必提交真的IP地址，否则会影响支付成功率。 | 32 | 否

#### 5.3.2. 应答消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
|  bank_card_num | 收款银行卡号：客户需要转账给此银行卡号。  | MAX(32) | 否 |
|  bank_acc_name | 收款银行户名：客户需要转账给此银行户名。  | 32 | 否 |
|  amount | 充值金额：数字，必须两位小数点，无千位符。  | 10 | 否 |
|  email | 收款Email账号：正确的Email格式，如果mode为2，则不能为空。  | 64 | 可 |
|  company_order_num | 商户订单号：唯一性的字符串  | 64 | 否 |
|  datetime | 订单有效时间：年月日时分秒。  | 14 | 否 |
|  note | 附言：用于订单匹配。  | 32 | 可 |
|  mownecum_order_num | 支付平台订单号：字符串，唯一。  | 64 | 否 |
|  status | 状态： <br>1：处理成功-success<br>0：处理失败-failed  | 1 | 否 |
|  error_msg | 错误信息：如果status为0，该参数需要显示信息。  | 128 | 可 |
|  mode | 收款方式：<br>1：银行账号 <br>2： Email收款  | 1 | 否 |
|  issuing_bank_address | 开户行地址：银行卡开户行地址。  | 64 | 可 |
|  break_url | 引用地址：银行卡充值该参数为空。  | 128 | 可 |
|  timeoutAt | 订单超时时间  | 13 | 否 |
|  deposit_mode  | 充值渠道：<br>34 - C2C支付宝到银行卡<br>43 - C2C卡到卡（无附言） | 1 | 否 |
|  collection_bank_id |收款银行ID：收款银行卡编码。| 2 | 否 |
|  collection_bank_name |收款银行名称：收款银行卡归属银行的名称。  | 128 | 否 |
|  deposit_page   | 支付页面：B2C网银转账时，此字段不能为空，商户拿到此字段后，用此字段替换掉当前页面的内容。  | 4096 | 否 |
|  key | 动态密钥：<br>MD5(MD5(config)+bank_card_num+bank_acc_name+<br>amount+email+company_order_num+datetime+note+<br>mownecum_order_num+status+error_msg+mode+<br>issuing_bank_address+break_url+deposit_mode+<br>collection_bank_id)<br>备注：config为静态密码  | 32 | 否 |

### 5.4. 直接获得加密币收币地址

目前仅支持 51-USDT转账、52-USDT转账(商户提交CNY金额，支付USDT)、56-USDT转账(仅bitpie)、57-USDT转账(仅bitpie，商户提交CNY金额，支付USDT) 四种支付方式

请求url:​https://gapi.llque.com/api/v2/mownecum/direct_pay_request

#### 5.4.1. 请求消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id  | 商户ID：支付平台提供给商户的编码。 | 3 | 否 |
| bank_id | 对于51、52、56、57支付方式，不需要此参数，传空字符串即可。 | 2 | 可 |
| amount | 充值金额：数字，必须两位小数点，无千位符。 | 10 | 否
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否
| company_user | 商户客户唯一ID：客户在商户前台使用的唯一性ID，字符串，可使加密的用户ID,请务必提交正确的数据，否则会影响支付成功率。 | 64 | 否
| estimated_payment_bank | 对于51、52、56、57支付方式，不需要此参数，传空字符串即可。 | 2 | 可
| deposit_mode | 充值渠道：<br>51-USDT转账<br>52-USDT转账(商户提交CNY金额，支付USDT)<br>56-USDT转账(仅bitpie)<br>57-USDT转账(仅bitpie，商户提交CNY金额，支付USDT) | 1 | 否
| group_id | 群组ID：支付平台自用参数，建议商户默认0。 | 8 | 否
| web_url | 商户网址：建议商户可传网站域名。 | 128 | 否
| memo | 备用字段。 | 128 | 可
| note | 附言：如果note_model为1，note不能为空。 | 32 | 可
| note_model | 订单匹配模式：<br>1：商户自传附言<br>2：支付平台附言<br>3：支付平台金额 <br>4：商户金额。<br>目前，建议商户设置为2。 | 1 | 可
| key | 动态密钥：<br>MD5(MD5(config)+company_id+bank_id+<br>amount+company_order_num+company_user+<br>estimated_payment_bank+deposit_mode+<br>group_id+web_url+memo+note+note_model)<br>备注：config为静态密码。 | 32 | 否
| terminal | 使用终端：<br>1：电脑端<br>2：手机端<br>3：平板<br>9：其他 | 1 | 否
| client_ip | 客户终端IP。<br>这是发起支付的客户所用终端的IP地址，请务必提交真的IP地址，否则会影响支付成功率。 | 32 | 否

#### 5.4.2. 应答消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
|  bank_card_num | 对于51、52、56、57支付方式，此字段返回空串。  | MAX(32) | 否 |
|  bank_acc_name | 对于51、52、56、57支付方式，此字段返回空串。  | 32 | 否 |
|  amount | 充值金额：数字，必须两位小数点，无千位符。  | 10 | 否 |
|  email | 收款Email账号：正确的Email格式，如果mode为2，则不能为空。  | 64 | 可 |
|  company_order_num | 商户订单号：唯一性的字符串  | 64 | 否 |
|  datetime | 订单有效时间：年月日时分秒。  | 14 | 否 |
|  note | 附言：用于订单匹配。  | 32 | 可 |
|  mownecum_order_num | 支付平台订单号：字符串，唯一。  | 64 | 否 |
|  status | 状态： <br>1：处理成功-success<br>0：处理失败-failed  | 1 | 否 |
|  error_msg | 错误信息：如果status为0，该参数需要显示信息。  | 128 | 可 |
|  mode | 收款方式：<br>1：银行账号 <br>2： Email收款  | 1 | 否 |
|  issuing_bank_address | 对于51、52、56、57支付方式，此字段返回空串。  | 64 | 可 |
|  break_url | 引用地址：银行卡充值该参数为空。  | 128 | 可 |
|  timeoutAt | 订单超时时间  | 13 | 否 |
|  deposit_mode  | 充值渠道：<br>51-USDT转账<br>52-USDT转账(商户提交CNY金额，支付USDT)<br>56-USDT转账(仅bitpie)<br>57-USDT转账(仅bitpie，商户提交CNY金额，支付USDT) | 1 | 否 |
|  collection_bank_id |对于51、52、56、57支付方式，此字段返回空串。| 2 | 否 |
|  collection_bank_name |对于51、52、56、57支付方式，此字段返回空串。  | 128 | 否 |
|  deposit_page   | 对于51、52、56、57支付方式，此字段返回空串。  | 4096 | 否 |
|  trc20_addr   | TRC20收款地址。  | 45 | 否 |
|  erc20_addr   | ERC20收款地址。  | 45 | 否 |
|  omni_addr   | OMNI收款地址。  | 45 | 否 |
|  key | 动态密钥：<br>MD5(MD5(config)+bank_card_num+bank_acc_name+<br>amount+email+company_order_num+datetime+note+<br>mownecum_order_num+status+error_msg+mode+<br>issuing_bank_address+break_url+deposit_mode+<br>collection_bank_id+trc20_addr+erc20_addr+omni_addr)<br>备注：config为静态密码  | 32 | 否 |


----------


## 6. 支付结果通知
### 6.1. 请求消息 
&emsp;&emsp;这是支付平台发给商户的消息，在确认支付成功后，用来告知商户订单的支付结 果，使用HTTP POST传递参数。

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| pay_time | 充值时间：支付平台系统从银行抓取到的客户转账时间，格式：年月日分秒。 | 14 | 否 |
| bank_id | 银行ID : 客户发起充值的银行ID,编码详情见附件。 | 2 | 否 |
| amount | 充值金额： 数字，必须两位小数点，无千位符。 | 10 | 否 |
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否 |
| mownecum_order_num | 支付平台订单号：字符串，唯一。 | 64 | 否 |
| pay_card_num | 付款卡卡号： 客户充值使用的付款卡号，数字串。 | 32 | 可 |
| pay_card_name | 付款卡用户名：客户充值使用的付款卡姓名。 | 32 | 可 |
| channel | 交易渠道： 网银或ATM转账。 | 32 | 可 |
| area | 交易地址 | 64 | 可 |
| fee | 手续费：客户付款交易手续费，直接来自银行的数字，某些银行抓不到转款手续费默认为0.00。 | 6 | 可 |
| transaction_charge | 服务费：支付平台收取单笔订单的服务费，数字。 | 6 | 可 |
| key | 动态密钥：<br>MD5(MD5(config)+pay_time+bank_id+amount<br>+company_order_num+mownecum_order_num+pay_card_num<br>+pay_card_name+channel+area+fee+transaction_charge<br>+deposit_mode)<br>备注：config为静态密码 | 32 | 否 |
| deposit_mode | 充值渠道：<br>24 - H5快捷支付<br>25 - 支付宝H5唤醒+PC扫码<br>26 - 支付宝商户扫码<br>27 - 微信扫码话费<br>28 - 微信固码扫码<br>29 - 网银转账<br>30 - 卡到卡（带附言）<br>31 - 卡到卡（无附言）<br>32 - 个码微信自由码<br>33 - 个码支付宝自由码<br>34 - C2C支付宝到银行卡<br>35 - 网银快捷<br>36 - 网银快捷（免签约）<br>37 - 微信H5<br>38 - C2C支付宝个码扫码（混合）<br>39 - 微信扫码混跑 <br>40 - H5快捷支付（一号一户）<br>41 - C2C微信自由码（混合）<br>42 - C2C微到卡<br>43 - C2C卡到卡（无附言）<br>44 - C2C卡到卡（带附言）<br>45 - C2C微信赞赏码<br>46 - C2C支付宝个码H5<br>51 - USDT转账<br>61 - USD电汇<br>71 - EUR电汇  | 1 | 否 |
| base_info | 渠道原始关键信息:银行充值的交易流水号。 | 256 | 可 |
| operating_time | 支付平台系统完成时间：格式，年月日时分秒。 | 14 | 否 |
### 6.2. 应答消息 
&emsp;&emsp;这是商户发给支付平台的消息，用来告知支付平台已收到 “支付结果通知”，请使 用json格式。

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_order_num | 商户订单号： 唯一性的字符串。 | 64 | 否 |
| mownecum_order_num | 支付平台订单号: 字符串，唯一。 | 64 | 否 |
| status | 商户响应状态：1 ：成功，0：失败 | 1 | 否 |
| error_msg | 错误信息：如果status为0 ,则该参数需要显示。 | 128 | 可 |


----------


## 7.支付请求参数范例

### 7.1. 微信扫码转账（话费）

#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190314151847&memo=&bank_id=1&web_url=https%3A%2F%2Fwww.bing.com&key=69a41365d607c074c58e3855c30353df&deposit_mode=27&terminal=2&company_user=1552547927.89&estimated_payment_bank=&company_id=fb&bank_card_num=&note=20190314151847&amount=10.00&group_id=0

&emsp;&emsp;**关键字段的取值：**

&emsp;&emsp;deposit_mode=27

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx


#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"10.00","email":"","company_order_num":"20190314151847","datetime":"","note":"","mownecum_order_num":"","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"http://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=19c625b-982b-41b7-aca4-601a5b42deb2", "deposit_mode":"","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"2a9d461d7022b95d8d4616b0e0bb843f"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=19c2625b-982b-41b7-aca4-601a5b42deb2

### 7.2. 网银转账

#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190319111544&memo=&bank_id=&web_url=https%3A%2F%2Fwww.bing.com&key=5fe9bcb4541de8d23ef891c3aed1fe59&deposit_mode=29&terminal=2&company_user=1552965344.53&estimated_payment_bank=&company_id=fb&bank_card_num=&note=0190319111544&amount=200.00&group_id=0

&emsp;&emsp;**关键字段的取值**：

&emsp;&emsp;deposit_mode=29

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx

#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"200.00","email":"","company_order_num":"20190319111544","datetime":"","note":"","mownecum_order_num":"","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"http://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=45e0b9b7-d821-4832-9b32-b1b046893dee", "deposit_mode":"","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"d0f75a63992f6f65d15d7ecf33fe2bd1"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=45e0b9b7-d821-4832-9b32-b1b046893dee

### 7.3. 微信扫码
 
#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190402183317&memo=&bank_id=&web_url=xx&key=05689f0ecc7821875c2c17c47e06a075&deposit_mode=8&terminal=2&company_user=1554201197.57&estimated_payment_bank=&company_id=xx&bank_card_num=&note=20190402183317&amount=1.00&group_id=0

&emsp;&emsp;**关键字段的取值：**

&emsp;&emsp;deposit_mode=28

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx

#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"1.00","email":"","company_order_num":"20190402183317","datetime":"","note":"","mownecum_order_num":"","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"http://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=8b80ca81-1e43-4ebf-a7c3-d3a7baddad6e6e", "deposit_mode":"","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"1607ed85cf5059c58ce1761df384dfb"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=8b80ca81-1e43-4ebf-a7c3-d3a7baddad6e6e

### 7.4. 支付宝扫码

#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190402190314&memo=&bank_id=&web_url=xx.com&key=9a042e382f52f330965464d513891e44&deposit_mode=25&terminal=2&company_user=1554202994.41&estimated_payment_bank=&company_id=xx&bank_card_num=&note=20190402190314&amount=1.00&group_id=0

&emsp;&emsp;**关键字段的取值：**

&emsp;&emsp;deposit_mode=25

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx

#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"1.00","email":"","company_order_num":"20190402190314","datetime":"","note":"","mownecum_order_num":"","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"http://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93caae-75f8-43c4-9f21-da32165ef224", "deposit_mode":"","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"e944a8188dee34b7d06b1ac30cbf2d"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93ca3ae-75f8-43c4-9f21-da32165ef224

### 7.5. USDT转账

#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190402190314&memo=&bank_id=&web_url=xx.com&key=9a042e382f52f330965464d513891e44&deposit_mode=51&terminal=2&company_user=1554202994.41&estimated_payment_bank=&company_id=xx&bank_card_num=&note=20190402190314&amount=1.00&group_id=0

&emsp;&emsp;**关键字段的取值：**

&emsp;&emsp;deposit_mode=51

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx

#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"1.00","email":"","company_order_num":"20190402190314","datetime":"","note":"","mownecum_order_num":"","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"http://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93caae-75f8-43c4-9f21-da32165ef224", "deposit_mode":"51","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"9a042e382f52f330965464d513891e44"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93ca3ae-75f8-43c4-9f21-da32165ef224

### 7.6. 外币电汇

#### &emsp;1. 请求消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;note_model=1&company_order_num=20190402190314&memo=&bank_id=&web_url=xx.com&key=184553fd29d37da365ade233f6b6aa71&deposit_mode=61&terminal=2&company_user=1554202994.41&estimated_payment_bank=&company_id=xx&bank_card_num=&note=20190402190314&amount=1.00&group_id=0

&emsp;&emsp;**关键字段的取值：**

&emsp;&emsp;deposit_mode=61

&emsp;&emsp;note_model=1

&emsp;&emsp;company_id=xx

#### &emsp;2. 应答消息

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"bank_card_num":"","bank_acc_name":"","amount":"1.00","email":"","company_order_num":"20190402190314","datetime":"","note":"","mownecum_order_num":"93fcbe5c-5628-45ab-a3eb-accea3fe48dd","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93ca3ae-75f8-43c4-9f21-da32165ef224","deposit_mode":"61","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"c5bd84bf8ebb227fa5bcf056b25d1525"}

&emsp;&emsp;需要显示给客户的信息：

&emsp;&emsp;break_url=https://gpay.llque.com/api/v2/mownecum/ui/pay_request?id=d93ca3ae-75f8-43c4-9f21-da32165ef224

----------


## 8. 交易结果查询
&emsp;&emsp;请求地址：​https://gapi.llque.com/api/v2/mownecum/pay_query​ 。

&emsp;&emsp;请求以FORM格式提交，返回为标准的JSON格式。
#### 8.1. 请求消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id | 商户ID：支付平台提供给商户的编码。 | 3 | 否 |
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否 |
| key | 动态密钥：<br>MD5(MD5(config)+company_id+company_order_num)<br>备注：config为静态密码。 | 32 | 否 |

#### 8.2. 应答消息
|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id | 商户ID：支付平台提供给商户的编码。 | 3 | 否 |
| company_order_num | 商户订单号：唯一性的字符串。 | 64 | 否 |
| mownecum_order_num | 支付平台订单号：字符串，唯一。 | 64 | 是 |
| pay_time | 充值时间：支付平台系统从银行抓取到的客户转账时间，格式：年月日分秒。 | 14 | 是 |
| amount | 支付平台订单号：字符串，唯一。 | 10 | 是 |
| transaction_charge | 服务费：支付平台收取单笔订单的服务费，数字。 | 6 | 是 |
| deposit_mode | 充值渠道：<br>24 - H5快捷支付<br>25 - 支付宝H5唤醒+PC扫码<br>26 - 支付宝商户扫码<br>27 - 微信扫码话费<br>28 - 微信固码扫码<br>29 - 网银转账<br>30 - 卡到卡（带附言）<br>31 - 卡到卡（无附言）<br>32 - 个码微信自由码<br>33 - 个码支付宝自由码<br>34 - C2C支付宝到银行卡<br>35 - 网银快捷<br>36 - 网银快捷（免签约）<br>37 - 微信H5<br>38 - C2C支付宝个码扫码（混合）<br>39 - 微信扫码混跑 <br>40 - H5快捷支付（一号一户）<br>41 - C2C微信自由码（混合）<br>42 - C2C微到卡<br>43 - C2C卡到卡（无附言）<br>44 - C2C卡到卡（带附言）<br>45 - C2C微信赞赏码<br>46 - C2C支付宝个码H5<br>51 - USDT转账<br>61 - USD电汇<br>71 - EUR电汇  | 1 | 是 |
| operating_time | 支付平台系统完成时间：格式，年月日时分秒 | 14 | 是 |
| pay_status | 订单状态：<br> 0：等待支付<br>1：支付成功<br>2：支付失败 |  |  |
| key | 动态密钥：MD5(MD5(config)+company_id+company_order_num + pay_status)<br>备注：config为静态密码。 | 14 | 是 |
| code | 查询状态：<br>200：表示查询成功<br>非200：表示查询失败 |  | 否 |
| error_message | 错误提示。 | 14 | 是 |

## 9. 查询 52/57通道 USDT汇率的接口
&emsp;&emsp;请求地址：​https://gapi.llque.com/api/merchant/admin/query_usdt_rate​ 

#### 9.1. 应答消息
|  参数名称   | 参数说明  | 
|  ----  | ----  |
| status | 查询状态：<br>200：表示查询成功<br>非200：表示查询失败 | 
| message | 返回信息。 | 
| buy_usdt | USDT买入价。 | 
| sell_usdt | USDT卖出价。 | 
| buy_usdt_with_usd | USDT/USD买入价。 | 
| sell_usdt_to_usd | USDT/USD卖出价。 | 
----------


## 10. 银行编码
| 银行名称  | 银行编码 | 银行缩写 |
| :----: | :----: | :----: |
| 工商银行 | 1 | ICBC |
| 招商银行 | 2 | CMBCHINA |
| 建设银行 | 3 | CCB |
| 农业银行 | 4 | ABC |
| 中国银行 | 5 | BOC |
| 交通银行 | 6 | BOCO |
| 民生银行 | 7 | CMBC |
| 中信银行 | 8 | ECITIC |
| 上海浦东发展银行 | 9 | SPDB |
| 中国邮政储蓄 | 10 | POST |
| 光大银行 | 11 | CEB |
| 广发银行 | 13 | CGB |
| 兴业银行 | 15 | CIB |
| 北京银行 | 70 | BCCB |
| 上海银行 | 71 | SHB |
| 华夏银行 | 72 | HXBANK |
| 宁波银行 | 73 | NBBANK |
| 恒生银行 | 74 | HSB |
| 恒丰银行 | 75 | EGB |
| 渣打银行 | 76 | ZDYH |
| 平安银行 | 77 | SZPA |
| 长沙银行 | 78 | BCS |
| 浙商银行 | CZ | CZ |